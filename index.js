var http = require('http'),
    router = new (require('./lib/router')),
    Auth = require('./lib/authorize'),
    MongoClient = require('mongodb').MongoClient,
    UsersProvider = require('./resources/providers/users-provider'),
    Users = require('./resources/users'),
    ChannelsProvider = require('./resources/providers/channels-provider'),
    Channels = require('./resources/channels');

function options (norm, auth) {
    return function (req, res) {
        var t = auth && Auth.match(req, req.stamps.username) ? auth : norm;
        res.setHeader('Access-Control-Allow-Methods', t.join(', '));
        res.end();
    };
}

function cors (pre) {
    if (pre) {
	return function (req, res, next) {
	    res.setHeader('Access-Control-Allow-Origin', '*');
	    res.setHeader('Access-Control-Allow-Headers', 'AUTHORIZATION');
	    next();
	};
    }

    return function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	next();
    };
}

Auth(process.env.AZUBU_HS256_KEY);

router.with('/users/:username')
    .get('?', Auth.check, cors(), Users.get)
    .get('/following/:follow?', cors(), Users.following)
    .get('/followed', cors(), Users.followed)
    .put('', Auth.restrict, cors(), Users.update)
    .post('/following/:follow', Auth.restrict, cors(), Users.follow)
    .delete('/following/:follow', Auth.restrict, cors(), Users.unfollow);

router.options('/users', cors(true), options(['GET']));
router.with('/users/:username')
    .options('', Auth.check, cors(true),  options(['GET'], ['GET', 'PUT']))
    .options('/following', cors(true), options(['GET']))
    .options('/following/:follow', Auth.check, cors(true), options(
	['GET'], ['GET', 'POST', 'DELETE']
    )).options('/followed', cors(true), options(['GET']));

router.with('/channels/:username')
    .get('?', cors(), Channels.get)
    .get('/:desc/:index?', cors(), Channels.bricks)
    .post('/:desc', Auth.restrict, cors(), Channels.add)
    .put('/:desc', Auth.restrict, cors(), Channels.update)
    .put('/:desc/:index', Auth.restrict, cors(), Channels.update)
    .delete('/:desc', Auth.restrict, cors(), Channels.remove)
    .delete('/:desc/:index', Auth.restrict, cors(), Channels.remove);

router.options('/channels', cors(true), options(['GET']));
router.with('/channels/:username')
    .options('', cors(true), options(['GET']))
    .options('/:desc', Auth.check, cors(true), function (req, res) {
	var a = 'Access-Control-Allow-Methods';
        if (req.auth && Auth.match(req, req.stamps.username)) {
            if (isNaN(parseInt(req.stamps.desc))) {
                res.setHeader(a, 'GET, POST');
            } else {
                res.setHeader(a, 'GET, PUT, DELETE');
            }
        } else {
            res.setHeader(a, 'GET');
        }
        res.end();
    }).options('/:desc/:index', Auth.check, cors(true), options(
	['GET'], ['GET', 'PUT', 'DELETE']
    ));

MongoClient.connect(process.env.AZUBU_MONGO_URL, function (err, db) {
    Users(new UsersProvider(process.env.AZUBU_NEO4J_URL));
    Channels(new ChannelsProvider(db.collection('bricks')));

    http.createServer(function (req, res) {
        req.body = '';
        req.on('data', function (c) {
            req.body += c.toString();
        });
        req.on('end', function () {
            if (req.body != '') {
                req.body = JSON.parse(req.body);
            }
            router.apply(req, res)();
        });
    }).listen(8080);
});
