var http = require('http');

function expandPath (tree, path, f) {
    var token = path.shift();

    tree[token] = path.length == 0 ? f : expandPath(tree[token] || {}, path, f);

    return tree;
}

function searchPath (tree, path, args) {
    var token = path.shift(), arr = [], match, i, rex;

    if (tree[token] instanceof Array) {
        return tree[token];
    } else if (tree[token]) {
        return searchPath(tree[token], path, args);
    }

    rex = /^:(\w+)(\?)?$/;
    if (path.length > 1 || (token != '@GET' && path.indexOf('@GET') == -1)) {
        rex = /^:(\w+)$/;
    }
    for (i in tree) {
        if (match = rex.exec(i)) {
            if (match[2] && token.indexOf('@') != -1) {
                args[match[1]] = null;
                path.unshift(token);
            } else {
                args[match[1]] = token;
            }

            return searchPath(tree[i], path, args);
        }
    }

    return null;
}

module.exports = function () {
    this.tree = {};
    this.prefix = '';
};

module.exports.prototype.with = function (pfx) {
    var res = new module.exports();

    res.tree = this.tree;
    res.prefix += pfx;

    return res;
};

['get', 'post', 'put', 'delete', 'options'].forEach(function (v) {
    module.exports.prototype[v] = function () {
        var i = 1, fns = [], path;

        if (arguments.length < 2 || typeof(arguments[0]) != 'string') {
            throw new Error('Invalid arguments: ' +
                'string, function[, function, ...] expected.');
        }

	path = this.prefix + arguments[0];
        path = path.split(/\//).slice(1).concat('@' + v.toUpperCase());
        while (i < arguments.length) fns.push(arguments[i++]);
        expandPath(this.tree, path, fns);

        return this;
    };
});

module.exports.prototype.apply = function (req, res) {
    var path, fs, cs = [], i;

    if (!(req instanceof http.IncomingMessage)) {
        throw new Error('http.IncomingMessage required in first argument.');
    }

    req.stamps = {};
    path = req.url.split(/\?/)[0].split(/\//).slice(1);
    path = path.concat('@' + req.method.toUpperCase());
    if((fs = searchPath(this.tree, path, req.stamps))) {
        for (i = fs.length - 1; i >= 0; --i) {
            (function (f, c) {
                cs.unshift(function () { return f.call(f, req, res, c); });
            })(fs[i], cs.length ? cs[0] : function () {});
        }
    }

    return cs.length ? cs[0] : null;
};
