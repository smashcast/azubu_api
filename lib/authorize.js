var crypto = require('crypto'),
    key;

function unauthorized (res) {
    res.setHeader('www-authenticate', 'bearer');
    res.statusCode = 401;
    res.end();
}

function getAuth (req) {
    var r = req.headers.authorization;

    if (r && r.indexOf('bearer') != -1) {
        return r.substring('bearer '.length).split('.');
    }
    return null;
}

function decode (auth) {
    var buffer = new Buffer(auth[1], 'base64');
    return JSON.parse(buffer.toString('utf8'));
}

function verify (auth) {
    var hmac = crypto.createHmac('sha256', key),
	bdy = JSON.parse(new Buffer(auth[1], 'base64').toString('utf8')),
	d = (new Date).valueOf();

    return hmac.update(auth[0] + '.' + auth[1]).digest('base64') == auth[2]
	&& bdy.iat < bdy.exp && bdy.iat < d && bdy.exp > d;
}

module.exports = function (k) {
    key = k;
};

module.exports.restrict = function (req, res, next) {
    var auth = getAuth(req);

    if (auth && auth.length == 3 && verify(auth)) {
        req.auth = decode(auth);
        return (next || function () {})();
    }

    return unauthorized(res);
};

module.exports.check = function (req, res, next) {
    var auth = getAuth(req);

    if (auth && auth.length == 3 && verify(auth)) {
        req.auth = decode(auth);
    }

    (next || function () {})();
};

module.exports.match = function (req, rhs) {
    var u;

    if (req.auth && rhs) {
        if (typeof(rhs) == 'string') {
            u = req.auth['http://azubu.tv/username'];
            return u && u.toLowerCase() == rhs;
        } else if (typeof(rhs) == 'number') {
            return req.auth.sub && req.auth.sub == rhs;
        }
    }

    return false;
};
