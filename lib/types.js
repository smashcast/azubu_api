var t, types = { 
    bio: { 
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        content: String,
        xFixed: Boolean,
        yFixed: Boolean
    },
    gear: {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        items: [
            {
                name: String,
                url: String,
                category: String,
                image: String,
                imageUrl: String
            }
        ]
    }, 
    html: {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        content: String
    }, 
    image: {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        item: {
            imageUrl: String,
            text: String,
            url: String
        }
    }, 
    news: {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        article: {
            title: String,
            headline: String,
            subHeadline: String,
            backgroundImageUrl: String,
            content: String,
            excerpt: String,
            excerptEnabled: Boolean,
            date: Date,
            author: String
        },
        xFixed: Boolean,
        yFixed: Boolean
    }, 
    profile: {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        item: {
            url: String
        }
    }, 
    social: {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        items: [
            {
                bgClass: String,
                type: String,
                url: String,
                imageUrl: String,
                socialName: String,
                icon: String
            }
        ]
    }, 
    sponsor: {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        items: [
            {
                url: String,
                imageUrl: String
            }
        ]
    }, 
    twitter: {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        twitterUrl: String,
        twitterWidgetId: String,
        twitterEmbedCode: String,
    }, 
    youtube: {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        item: {
            url: String,
            videoId: String
        }
    }, 
    donor: {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        item: {
            code: String,
            isHosted: Boolean,
            imageUrl: String
        }
    }
};

types.recent 
    = types.achievements 
    = types.summoner 
    = types.statistics 
    = {
        position: { 
            sizeX: Number, 
            sizeY: Number, 
            row: Number, 
            col: Number
        },
        state: {
            locked: Boolean
        },
        title: {
            text: String, 
            icon: String, 
            iconColor: String
        },
        topOffset: String,
        xScrollBar: String,
        yScrollBar: String,
        active: Boolean,
        showTitle: Boolean,
        item: {
            summonerId: String,
            region: String,
            isBuilt: Boolean,
            dataUrl: String
        }
    };

function match (type, obj) {
    var i, k;
    
    for (i in obj) {
        if (!type[i]) {
            return false;
        } else if (obj[i] instanceof Array && type[i] instanceof Array) {
            for (k = 0; k < obj[i].length; ++k) {
                if (!match(type[i][k % type[i].length], obj[i][k])) {
                    return false;
                }
            }
        } else if (obj[i] instanceof Object) {
            if (type[i] instanceof Function && obj[i] instanceof type[i]) {
                continue;
            } else if (!match(type[i], obj[i])) {
                return false;
            }
        } else if (typeof(obj[i]) != typeof((new type[i]).valueOf())) {
            if ((new type[i](obj[i])).valueOf()) {
                continue;
            }
            return false;
        }
    }
    
    return true;
}

module.exports.match = match;

for (t in types) {
    (function (type) {
        module.exports[type] = function (obj) {
            return match(types[type], obj);
        }
    })(t);
}