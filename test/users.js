var assert = require('assert'),
    neo4j = require('neo4j'),
    connection = 'http://neo4j:neo4j@neo4j.dev.azubu.it:7474',
    db = new neo4j.GraphDatabase(connection),
    Users = require('../resources/users.js'),
    UsersProvider = require('../resources/providers/users-provider.js');
    
function createMessage (path, stamps, data) {
    return {
        url: path,
        stamps: stamps,
        body: data
    };
}

function retrieving () {
    var list, count, faker, following, followed, fcount;
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (u:User) ' + 
                'RETURN u.id AS id, u.username AS username LIMIT {l};',
            params: { l: 100 }
        }, function (err, res) {
             list = res;
             done();
        });
    });
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (u:User) WITH u RETURN count(u) AS c;'
        }, function (err, res) {
            count = res[0].c;
            done();
        })
    });
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (u:User {searchName: {u}}) ' +
                'RETURN u.id AS id, u.username AS username, u.email AS email;',
            params: { u: 'faker' }
        }, function (err, res) {
            faker = res[0];
            done();
        });
    });
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (:User {searchName: {u}})-[:Following]->(u:User) ' +
                'RETURN u.username AS username, u.id AS id;',
            params: { u: 'mikiocute' }
        }, function (err, res) {
            following = res;
            done();
        });
    });
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (u:User)-[:Following]->(:User {searchName: {u}}) ' +
                'RETURN u.username AS username, u.id AS id LIMIT 5;',
            params: { u: 'faker' }
        }, function (err, res) {
            followed = res;
            done();
        });
    });
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (u:User)-[:Following]->(:User {searchName: {u}}) ' +
                'WITH u RETURN count(u) AS c;',
            params: { u: 'faker' }
        }, function (err, res) {
            fcount = res[0].c;
            done();
        });
    });
    
    it('Returns a list of users.', function (done) {
        Users.get(createMessage('/users', {}), {
            end: function (r) {
                assert.equal(JSON.stringify(list), r);
                done();
            }
        });
    });
    it('Returns lists of users with limit and skip params.', function (done) {
        Users.get(createMessage('/users?skip=20&limit=5', {}), {
            end: function (r) {
                var o = JSON.parse(r), i = 20;
                                
                for (; i < 25; ++i) {
                    assert.deepEqual(list[i], o[i - 20]);
                }
                done();
            }
        });
    });
    it('Returns a specific user\'s data.', function (done) {
        var name = list[0].username;
        Users.get(createMessage('/users/' + name, {
            username: name
        }), {
            end: function (r) {
                assert.deepEqual(list[0], JSON.parse(r));
                done();
            }
        });
    });
    it('Returns all user\'s data when authorized.', function (done) {
        var req = createMessage('/users/faker', {
            username: 'faker'
        });
        req.auth = {
            sub: '313614',
            'http://azubu.tv/username': 'Faker'
        };
        Users.get(req, {
            end: function (r) {
                assert.deepEqual(faker, JSON.parse(r));
                done();
            }
        });
    });
    it('Provides an appropriate error code on missing users.', function (done) {
        var res = {
            end: function () {
                assert.equal(res.statusCode, 404);
                done();
            }
        };
        Users.get(createMessage('/users/not_fount', {
            username: 'not_found'
        }), res);
    });
    it('Returns a count of all users.', function (done) {
        this.timeout(3000);
        Users.get(createMessage('/users?count', {}), {
            end: function (r) {
                assert.equal(JSON.stringify({ count: count }), r);
                done();
            }
        });
    });
    it('Returns a list of users followed by a given user.', function (done) {
        Users.following(createMessage('/users/mikiocute/following?limit=5', { 
            username: 'mikiocute'
        }), {
            end: function (r) {
                var o = JSON.parse(r), i = 0;
                
                for (; i < 5 && i < o.length; ++i) {
                    assert.deepEqual(following[i], o[i]);
                }
                done();
            }
        });
    });
    it('Returns a specific user followed by a given user.', function (done) {
        var name = following[0].username,
            req = createMessage('/users/mikiocute/following/' + name, { 
                username: 'mikiocute',
                follow: name
            });
        Users.following(req, {
            end: function (r) {
                assert.deepEqual(following[0], JSON.parse(r));
                done();
            }
        });
    });
    it('Provides an appropriate error on missing following.', function (done) {
        var req = createMessage('/users/mikiocute/following/not_found', {
            username: 'mikiocute', follow: 'not_found'
        }), res = {
            end: function () {
                assert.equal(res.statusCode, 404);
                done();
            }
        };
        Users.following(req, res);
    });
    it('Returns a list of users following a given user.', function (done) {
        var req = createMessage('/users/faker/followed?limit=5', {
            username: 'faker'
        });
        Users.followed(req, {
            end: function (r) {
                assert.deepEqual(followed, JSON.parse(r));
                done();
            }
        });
    });
    it('Returns a count of users following a given user.', function (done) {
        Users.followed(createMessage('/users/faker/followed?count', {
            username: 'faker'
        }), {
            end: function (r) {
                assert.equal(JSON.stringify({ count: fcount }), r);
                done();
            }
        });
    });
}

function modifying () {
    after(function (done) {
        db.cypher({
            query: 'MATCH (:User {searchName: {u}})-[f:Following]->() ' +
                'WITH f DELETE f;',
            params: { u: 'unit_tester' }
        }, done);
    });
    
    it('Allows authorized users to update via PUT method.', function (done) {
        var req = createMessage('/users/unit_tester', 
            { username: 'unit_tester' }, { email: 'tester@unit.com' });
        
        req.auth = {
            sub: '-1',
            'http://azubu.tv/username': 'unit_tester'
        };
        Users.update(req, {
            end: function (r) {
                assert.equal(JSON.parse(r).email, 'tester@unit.com');
                done();
            }
        });
    });
    it('Denies unauthorized users updating a record.', function (done) {
        var req = createMessage('/users/faker', 
                { username: 'faker' }, { email: 'faker@faker.com' }),
            res = {
                end: function () {
                    assert.equal(res.statusCode, 401);
                    done();
                }
            };
        Users.update(req, res);
    });
    it('Provides an appropriate error code on missing puts.', function (done) {
        //This is somewhat contrived.
        var req = createMessage('/users/not_found', {
            username: 'not_found'
        }), res = {
            end: function () {
                assert.equal(res.statusCode, 404);
                done();
            }
        };
        req.auth = {
            'http://azubu.tv/username': 'not_found'
        };
        Users.update(req, res);
    });
    it('Allows authorized users to follow others.', function (done) {
        var req = createMessage('/users/unit_tester/following/faker', {
            username: 'unit_tester', follow: 'faker'
        }), res = {};
        
        req.auth = {
            sub: '-1',
            'http://azubu.tv/username': 'unit_tester'
        };
        res.end = function () {
            assert.equal(res.statusCode, 201);
            db.cypher({
                query: 'MATCH (:User {searchName: {u}})' + 
                    '-[f:Following]->(:User {searchName: {f}}) RETURN f;',
                params: { u: 'unit_tester', f: 'faker' }
            }, function (err, res) {
                assert(res.length);
                done();
            });
        };
        
        Users.follow(req, res);
    });
    it('Denies unauthorized users following another.', function (done) {
        var req = createMessage('/users/faker/following/mikiocute', 
                { username: 'faker', follow: 'mikiocute' }),
            res = {
                end: function () {
                    assert.equal(res.statusCode, 401);
                    done();
                }
            };
        Users.follow(req, res);
    });
    it('Provides an appropritate error code on missing post.', function (done) {
        var req = createMessage('/users/unit_tester/following/not_found', {
                username: 'unit_tester', follow: 'not_found'
            }), res = {
                end: function () {
                    assert.equal(res.statusCode, 404);
                    done();
                }
            };
        req.auth = {
            sub: '-1',
            'http://azubu.tv/username': 'unit_tester'
        };
        Users.follow(req, res);
    });
    it('Allows authorized users to unfollow others.', function (done) {
        var req = createMessage('/users/unit_tester/following/faker', {
            username: 'unit_tester', follow: 'faker'
        }), res = {
            end: function () {
                assert.equal(res.statusCode, 204);
                db.cypher({
                    query: 'MATCH (:User {searchName: {u}})' + 
                        '-[f:Following]->(:User {searchName: {f}}) RETURN f;',
                    params: { u: 'unit_tester', f: 'faker' }
                }, function (err, res) {
                    assert(!res.length);
                    done();
                });
            }
        };
        
        req.auth = {
            sub: '-1',
            'http://azubu.tv/username': 'unit_tester'
        };
        
        Users.unfollow(req, res);
    });
    it('Denies unauthorized users unfollowing another.', function (done) {
        var req = createMessage('/users/faker/following/mikiocute', { 
            username: 'faker', follow: 'mikiocute' 
        }), res = {
            end: function () {
                assert.equal(res.statusCode, 401);
                done();
            }
        };
        Users.unfollow(req, res);
    });
    it('Provides an appropritate error on missing delete.', function (done) {
        var req = createMessage('/users/faker/following/not_found', {
            username: 'faker', follow: 'not_found'
        }), res = {
            end: function () {
                assert.equal(res.statusCode, 404);
                done();
            }
        };
        req.auth = {
            sub: '313614',
            'http://azubu.tv/username': 'Faker'
        };
        Users.follow(req, res);
    });
}

describe('Users', function () {
    Users(new UsersProvider(connection));
    
    it('Is well-defined.', function () {
        assert.notEqual(Users, undefined);
        assert.notEqual(Users, null);
    });
    describe('Retrieving', retrieving);
    describe('Modifying', modifying);
});