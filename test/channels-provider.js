var assert = require('assert'),
    ChannelsProvider = require('../resources/providers/channels-provider'),
    connection = process.env.AZUBU_MONGO_URL,
    mongo = require('mongodb'),
    bricks = require('./resources/bricks'),
    group = { 
        $group: { 
            _id: '$userId', username: { $first: '$username' }
        } 
    }, ands = {
        bio: function (b) {
            assert.equal(b.content, '<p>Testing bio.</p>');
        },
        gear: function (b) {
            assert.equal(b.items[0].name, 'A Thing');
        },
        html: function (b) {
            assert.equal(b.content, '<p>Testing html.</p>');
        },
        image: function (b) {
            assert.equal(b.item.url, 'http://some.image.link');
        },
        news: function (b) {
            assert.equal(b.article.headline, 'Unit Testing');
        },
        profile: function (b) {
            assert.equal(b.item.url, 'http://some.image.url');
        },
        social: function (b) {
            var url = 'https://www.facebook.com/unit_test';
            assert.equal(b.items[0].url, url);
        },
        sponsor: function (b) {
            assert.equal(b.items[0].url, 'http://some.test.url');
        },
        twitter: function (b) {
            assert.equal(b.twitterUrl, 'unit_test');
        },
        donor: function (b) {
            assert.equal(b.item.imageUrl, 'http://some.image.url');
        }
    };
    
['recent', 'achievements', 'summoner', 'statistics'].forEach(function (t) {
    ands[t] = function (b) {
        assert(b.item.dataUrl, 'http://some.data.url');
    };
});

describe('Channels Provider', function () {
    var channels, collection, n = 'unit_tester', b = { bad: 'brick' };
    
    before(function (done) {
        mongo.MongoClient.connect(connection, function (e, db) {
            collection = db.collection('bricks');
            channels = new ChannelsProvider(collection);
            done();
        });
    });
    
    it('Is well-defined.', function () {
        assert.notEqual(channels, undefined);
        assert.notEqual(channels, null);
    });
    describe('Retrieving', function () {
        var list, faker;
  
        before(function (done) {
            collection.aggregate([
                { $group: { 
                    _id: '$searchName',
                    uid: { $first: '$userId' }, 
                    username: { $first: '$username' } 
                } }, 
                { $sort: { _id: 1 } },
                { $project: { 
                    id: '$uid', 
                    username: '$username', 
                    _id: 0 
                } }
            ], function (err, res) {
                list = res;
                collection.find(
                    { searchName: 'faker' }, { _id: 0, searchName: 0 }
                ).sort({ _id: 1 }).toArray(function (err, res) {
                    faker = res;
                    done();
                });
            });
        });
        it('Returns a channel count.', function (done) {
            channels.count(function (e, c) {
                assert(!e);
                assert.equal(typeof(c), 'number');
                assert.equal(c, list.length);
                done();
            });
        });
        it('Returns a channel list.', function (done) {
            channels.list(function (e, c) {
                var i = 0;
                
                assert(!e);
                assert.equal(c.length, 5);
                for (; i < 5; ++i) {
                    assert.deepEqual(c[i], list[i + 10]);
                }
                done();
            }, 10, 5);
        });
        it('Limits channels list length by default.', function (done) {
            channels.list(function (e, c) {
                var i = 0;
                
                assert(!e);
                assert(c.length <= 100);
                for (; i < c.length; ++i) {
                    assert.deepEqual(c[i], list[i]);
                }
                done();
            });
        });
        it('Returns appropriate channels.', function (done) {
            channels.detail('faker', function (e, c) {
                assert(!e);
                assert.deepEqual(faker, c);
                done();
            });
        });
        it('Returns a particular channel\'s brick by index.', function (done) {
            channels.brick('faker', 2, function (e, b) {
                assert.deepEqual(b, faker[1]);
                done();
            });
        });
        it('Returns all channel bricks of a given type.', function (done) {
            channels.bricksByType('faker', 'image', function (e, b) {
                var i = 0, k = 0;
                
                for (; i < faker.length; ++i) {
                    if (faker[i].type == 'image') {
                        assert.deepEqual(faker[i], b[k++]);
                    }
                }
                done();
            });
        });
        it('Returns a channel\'s brick by type and index.', function (done) {
            channels.brickByType('faker', 'image', 2, function (e, b) {
                var i = 0, k = 0;
                
                for(; i < faker.length; ++i) {
                    if (faker[i].type == 'image' && ++k == 2) {
                        break;
                    }
                }
                assert.deepEqual(b, faker[i]);
                done();
            });
        });
    });
    describe('Adding', function () {
        var t, l = 0;
        
        after(function (done) {
            collection.remove({ searchName: n }, done);
        });
        
        for (t in ands) {
            (function (l) {
                it('Allows the adding of ' + t + ' bricks.', function (done) {
                    var b = JSON.parse(JSON.stringify(bricks[t]));
                    channels.addBrick(n, t, b, function (e, u) {
                        assert.equal(u.length, l);
                        assert.equal(u[0].type, t);
                        ands[t](u[0]);
                        done();
                    });
                });
                it('Rejects malformed ' + t + ' bricks.', function (done) {
                    channels.addBrick(n, t, b, function (e, u) {
                        assert.equal(e, 'Malformed brick of type: ' + t + '.');
                        assert(!u);
                        done();
                    });
                });
            })(++l);
        }
    });
    describe('Updating', function () {
        var t, i, bs;
        
        before(function (done) {
            var ins = [];
            
            for (t in ands) {
                ins.push({ searchName: n, type: t });
            }
            collection.insert(ins, function (err, res) {
                collection.find({ searchName: n })
                    .sort({ _id: 1 })
                    .toArray(function (err, res) {
                        bs = res;
                        done();
                    });
            });
        });
        
        after(function (done) {
            collection.remove({ searchName: n }, done);
        });
        
        for (t in ands) {
            it('Updates ' + t + ' bricks by index.', function (done) {
                var b = JSON.parse(JSON.stringify(bricks[t])), 
                    s = { }, i;
                    
                for (i = 0; i < bs.length && bs[i].type != t; ++i);
                s.index = i + 1;
                channels.updateBrick(n, s, b, function (err, res) {
                    assert.equal(res.length, bs.length);
                    assert.equal(res[i].type, t);
                    ands[t](res[i]);
                    done();
                });
            });
            it('Updates ' + t + ' bricks by index and type.', function (done) {
                var b = JSON.parse(JSON.stringify(bricks[t])),
                    s = { index: 1, type: t }, i;
                
                for (i = 0; i < bs.length && bs[i].type != t; ++i);
                channels.updateBrick(n, s, b, function (err, res) {
                    assert.equal(res.length, bs.length);
                    assert.equal(res[i].type, t);
                    ands[t](res[i]);
                    done();
                });
            });
            it('Rejects updating of non-existent bricks.', function (done) {
                var s = { index: bricks.length + 1 };
                
                channels.updateBrick(n, s, b, function (err, res) {
                    assert.equal(err, 'Invalid brick.');
                    assert(!res);
                    done();
                });
            });
            it('Rejects malformed ' + t + ' bricks.', function (done) {
                var s = { }, i;
                
                for (i = 0; i < bs.length && bs[i].type != t; ++i);
                s.index = i + 1;
                channels.updateBrick(n, s, b, function (err, res) {
                    assert.equal(err, 'Malformed brick of type: ' + t + '.');
                    assert(!res);
                    done();
                });
            });
        }
    });
    describe('Removing', function () {
        var l;
        
        before(function (done) {
            var ins = [];
            
            for (t in ands) {
                ins.push({ searchName: n, type: t });
            }
            l = ins.length;
            collection.insert(ins, done);
        });
        
        after(function (done) {
            collection.remove({ searchName: n }, done);
        });
        
        it('Deletes bricks by index.', function (done) {
            channels.deleteBrick(n, { index: 1 }, function (err, res) {
                assert.equal(res.length, l - 1);
                done();
            });
        });
        it('Deletes bricks by type and index.', function (done) {
            var s = { index: 1, type: 'image' };
            channels.deleteBrick(n, s, function (err, res) {
                assert.equal(res.length, l - 2);
                done();
            });
        });
        it('Rejects the removal of non-existent bricks.', function (done) {
            channels.deleteBrick(n, { index: l }, function (err, res) {
                assert.equal(err, 'Invalid brick.');
                assert(!res);
                done();
            });
        });
    });
});
