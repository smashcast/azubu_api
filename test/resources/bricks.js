module.exports = {
    bio: {
        xFixed: false,
        title: {
            text: 'Unit Testing',
            iconColor: 'yellow',
            icon: 'fa-user'
        },
        yScrollBar: 'scroll',
        content: '<p>Testing bio.</p>',
        topOffset: '50px',
        state: {
            locked: true
        },
        position: {
            col: 0,
            sizeX: 5,
            sizeY: 5,
            row: 0
        },
        xScrollBar: 'hidden',
        yFixed: false,
        active: true,
        showTitle: true
    },
    gear: {
        title: {
            text: 'Unit Testing',
            iconColor: 'yellow',
            icon: 'fa-cog'
        },
        items: [
            {
                category: 'Stuff',
                name: 'A Thing'
            }
        ],
        yScrollBar: 'scroll',
        topOffset: '50px',
        state: {
            locked: false
        },
        position: {
            col: 0,
            sizeX: 4,
            sizeY: 8,
            row: 0
        },
        xScrollBar: 'hidden',
        active: true,
        showTitle: true
    },
    html: {
        position: {
            sizeX: 4,
            sizeY: 6,
            row: 0,
            col: 0
        },
        state: {
            locked: false
        },
        title: {
            text: 'Unit Testing',
            icon: 'fa-pencil-square-o',
            iconColor: 'yellow'
        },
        content: '<p>Testing html.</p>',
        topOffset: '50px',
        xScrollBar: 'hidden',
        yScrollBar: 'scroll',
        active: true,
        showTitle: true,
    },
    image: {
        position: {
            sizeX: 3,
            sizeY: 6,
            row: 0,
            col: 0
        },
        state: {
            locked: false
        },
        title: {
            text: 'Unit Testing',
            icon: 'fa-user',
            iconColor: 'yellow'
        },
        item: {
            imageUrl: 'http://some.image.url',
            url: 'http://some.image.link',
            text: ''
        },
        topOffset: '50px',
        xScrollBar: 'hidden',
        yScrollBar: 'hidden',
        active: true,
        showTitle: true,
    },
    news: {
        position: {
            sizeX: 4,
            sizeY: 0,
            row: 0,
            col: 0
        },
        state: {
            locked: true
        },
        title: {
            text: 'Unit Testing',
            icon: 'fa-users',
            iconColor: 'yellow'
        },
        article: {
            headline: 'Unit Testing',
            subHeadline: 'Testing',
            backgroundImageUrl: 'http://some.image.url',
            excerptEnabled: true,
            date: '2015-09-01T10:32:09.362Z',
            author: 'unit_tester'
        },
        topOffset: 0,
        xScrollBar: 'hidden',
        yScrollBar: 'scroll',
        xFixed: false,
        yFixed: false,
        active: true,
        showTitle: true
    },
    profile: {
        position: {
            sizeX: 3,
            sizeY: 8,
            row: 0,
            col: 0
        },
        state: {
            locked: true
        },
        title: {
            text: 'Unit Testing',
            icon: 'fa-user',
            iconColor: 'yellow'
        },
        item: {
            url: 'http://some.image.url'
        },
        topOffset: '50px',
        xScrollBar: 'hidden',
        yScrollBar: 'hidden',
        active: true,
        showTitle: true
    },
    social: {
        title: {
            text: 'Unit Testing',
            iconColor: 'yellow',
            icon: 'fa-globe'
        },
        items: [
            {
                bgClass: 'facebook-background',
                socialName: 'unit test',
                url: 'https://www.facebook.com/unit_test',
                imageUrl: 'http://some.image.url',
                type: 'facebook',
                icon: 'fa-facebook'
            }
        ],
        yScrollBar: 'hidden',
        topOffset: '50px',
        state: {
            locked: false
        },
        position: {
            col: 0,
            sizeX: 3,
            sizeY: 5,
            row: 0
        },
        xScrollBar: 'hidden',
        active: true,
        showTitle: true
    },
    sponsor: {
        title: {
            text: 'Unit Testing',
            iconColor: 'yellow',
            icon: 'fa-user'
        },
        items: [
            {
                url: 'http://some.test.url',
                imageUrl: 'http://some.image.url'
            }
        ],
        yScrollBar: 'scroll',
        topOffset: '50px',
        state: {
            locked: false
        },
        position: {
            col: 7,
            sizeX: 0,
            sizeY: 0,
            row: 24
        },
        xScrollBar: 'hidden',
        active: true,
        showTitle: true
    },
    twitter: {
        title: {
            iconColor: 'yellow',
            text: 'Unit Testing',
            icon: 'fa-twitter'
        },
        twitterUrl: 'unit_test',
        yScrollBar: 'scroll',
        topOffset: '50px',
        state: {
            locked: false
        },
        twitterWidgetId: '1',
        position: {
            col: 4,
            sizeX: 0,
            sizeY: 0,
            row: 8
        },
        xScrollBar: 'hidden',
        active: true,
        showTitle: true
    },
    donor: {
        position: {
            sizeX: 11,
            sizeY: 4,
            row: 0,
            col: 0
        },
        state: {
            locked: false
        },
        title: {
            text: 'Unit Testing',
            icon: 'fa-user',
            iconColor: 'yellow'
        },
        item: {
            code: null,
            isHosted: false,
            imageUrl: 'http://some.image.url'
        },
        topOffset: '50px',
        xScrollBar: 'hidden',
        yScrollBar: 'hidden',
        active: true,
        showTitle: true
    }
};
module.exports.recent = module.exports.achievements = 
module.exports.summoner = module.exports.statistics =
{
    position: {
        sizeX: 8,
        sizeY: 20,
        row: 0,
        col: 0
    },
    state: {
        locked: false
    },
    title: {
        text: 'Unit Testing',
        icon: 'fa-globe',
        iconColor: 'yellow'
    },
    item: {
        summonerId: 'unit_tester',
        region: 'UT',
        isBuilt: true,
        dataUrl: 'http://some.data.url'
    },
    topOffset: '50px',
    xScrollBar: 'hidden',
    yScrollBar: 'scroll',
    active: true,
    showTitle: true
};