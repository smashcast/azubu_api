var assert = require('assert'),
    Channels = require('../resources/channels'),
    ChannelsProvider = require('../resources/providers/channels-provider')
    connection = 'mongodb://dev-mongodb1.azubu.it:27017/tpo_demo2',
    mongo = require('mongodb'),
    bricks = require('./resources/bricks'),
    simpleProject = { 
        $project: { 
            _id: 0, 
            username: 1, 
            id: '$user_id' 
        }
    },
    detailProject = { 
        $project: { 
            _id: 0, 
            username: 1, 
            id: '$user_id',
            bricks: 1
        }
    };

function createMessage (path, stamps, data) {
    return {
        url: path,
        stamps: stamps,
        body: data
    };
}

describe('Channels', function () {
    var collection, n = 'unit_tester';
    
    before(function (done) {
        mongo.MongoClient.connect(connection, function (err, db) {
            collection = db.collection('bricks');
            Channels(new ChannelsProvider(collection));
            done();
        });
    });
    
    it('Is well-defined.', function () {
        assert.notEqual(Channels, undefined);
        assert.notEqual(Channels, null);
    });
    describe('Retrieving', function () { 
        var list, faker;
        
        before(function (done) {
            collection.aggregate([
                { $group: { 
                    _id: '$searchName',
                    uid: { $first: '$userId' }, 
                    username: { $first: '$username' } 
                } }, 
                { $sort: { _id: 1 } },
                { $limit: 100 },
                { $project: { 
                    id: '$uid', 
                    username: '$username', 
                    _id: 0 
                } }
            ], function (err, res) {
                list = res;
                collection.find(
                    { searchName: 'faker' }, { _id: 0, searchName: 0 }
                ).sort({ _id: 1 }).toArray(function (err, res) {
                    faker = res;
                    done();
                });
            });
        });
        
        it('Returns a count of all channels.', function (done) {
            this.timeout(10000);
            Channels.get(createMessage('/channels?count', {}), {
                end: function (r) {
                    assert.equal(JSON.stringify({ count: list.length }), r);
                    done();
                }
            });
        });
        it('Returns a list of channels.', function (done) {
            Channels.get(createMessage('/channels', {}), {
                end: function (r) {
                    assert.equal(JSON.stringify(list), r);
                    done();
                }
            });
        });
        it('Returns lists of channels with params.', function (done) {
            Channels.get(createMessage('/channels?skip=10&limit=5', {}), {
                end: function (r) {
                    var o = JSON.parse(r), i = 10, k = 0;
                    
                    for (; i < 15 && i < r.length; ++i) {
                        assert.deepEqual(list[i], o[k++]);
                    }
                    done();
                }
            });
        });
        it('Returns a specific channel\'s data.', function (done) {
            Channels.get(createMessage('/channels/faker', { 
                username: 'faker' 
            }), {
                end: function (r) {
                    assert.equal(JSON.stringify(faker), r);
                    done();
                }
            });
        });
        it('Returns no content on missing channels.', function (done) {
            var req = createMessage('/channels/not_found', { 
                username: 'not_found' 
            }), res = {
                end: function () {
                    assert.equal(res.statusCode, 204);
                    done();
                }
            };
            
            Channels.get(req, res);
        });
        it('Returns a specific brick by index.', function (done) {
            var req = createMessage('/channels/faker/2', {
                username: 'faker',
                desc: '2'
            });
            
            Channels.bricks(req, {
                end: function (r) {
                    assert.equal(JSON.stringify(faker[1]), r);
                    done();
                }
            });
        });
        it('Returns a 404 on brick index out of bounds.', function (done) {
            var reqs = 0, oob = (list.length + 1).toString();
            
            function f () {
                assert.equal(this.statusCode, 404);
                if (++reqs == 2) {
                    done();
                }
            }
            
            Channels.bricks(createMessage('/channels/faker/0', {
                username: 'faker',
                desc: '0'
            }), { end: f });
            Channels.bricks(createMessage('/channels/faker/' + oob, {
                username: 'faker',
                desc: oob
            }), { end: f });
        });
        it('Returns all bricks of a specific type.', function (done) {
            var req = createMessage('/channels/faker/image', {
                username: 'faker',
                desc: 'image'
            });
            
            Channels.bricks(req, {
                end: function (r) {
                    var i = 0, k = 0, o = JSON.parse(r);
                    
                    for (; i < faker.length; ++i) {
                        if (faker[i].type == 'image') {
                            assert.deepEqual(faker[i], o[k++]);
                        }
                    }
                    done();
                }
            });
        });
        it('Returns a 404 on bad brick type.', function (done) {
            var req = createMessage('/channels/faker/fail', {
                username: 'faker',
                desc: 'fail'
            }), res = {
                end: function () {
                    assert.equal(res.statusCode, 404);
                    done();
                }
            };
            
            Channels.bricks(req, res);
        });
        it('Returns a brick given a type and index.', function (done) {
            var req = createMessage('/channels/faker/image/2', {
                username: 'faker',
                desc: 'image',
                index: '2'
            });
            
            Channels.bricks(req, {
                end: function (r) {
                    var i = 0, k = 0, o = JSON.parse(r);
                    
                    for (; i < faker.length; ++i) {
                        if (faker[i].type == 'image') {
                            ++k;
                        }
                        if (k == 2) {
                            assert.deepEqual(faker[i], o);
                            break;
                        }
                    }
                    done();
                }
            });
        });
        it('Returns a 404 given a type and invalid index.', function (done) {
            var req = createMessage('/channels/faker/image/0', {
                username: 'faker',
                desc: 'image',
                index: '0'
            }), res = {
                end: function () {
                    assert.equal(res.statusCode, 404);
                    done();
                }
            };
            
            Channels.bricks(req, res);
        });
    });
    describe('Modifying', function () {
        before(function (done) {
            collection.insert([
                { searchName: n, type: 'image' },
                { searchName: n, type: 'social' },
                { searchName: n, type: 'image' }
            ], done);
        });
        
        after(function (done) {
            collection.remove({ searchName: n }, done);
        });
        
        it('Allows updating a brick by index.', function (done) {
            var req = createMessage('/channels/' + n + '/1', {
                username: n,
                desc: '1'
            }, JSON.parse(JSON.stringify(bricks.image)));
            
            req.auth = {
                sub: -1,
                'http://azubu.tv/username': n
            };
            
            Channels.update(req, {
                end: function (r) {
                    r = JSON.parse(r);
                    assert.equal(r.length, 3);
                    assert.equal(r[0].item.url, bricks.image.item.url);
                    done();
                }
            });
        });
        it('Allows updating a brick by index and type.', function (done) {
            var req = createMessage('/channels/' + n + '/image/2', {
                username: n,
                desc: 'image',
                index: '2'
            }, JSON.parse(JSON.stringify(bricks.image)));
            
            req.auth = {
                sub: -1,
                'http://azubu.tv/username': n
            };
            
            Channels.update(req, {
                end: function (r) {
                    r = JSON.parse(r);
                    assert.equal(r.length, 3);
                    assert.equal(r[2].item.url, bricks.image.item.url);
                    done();
                }
            });
        });
        it('Prevents unauthorized users from updating.', function (done) {
            var req = createMessage('/channels/' + n + '/1', {
                username: n,
                desc: '1'
            }, JSON.parse(JSON.stringify(bricks.image))), res = {
                end: function () {
                    assert.equal(res.statusCode, 401);
                    done();
                }
            };
            
            req.auth = {
                sub: '313614',
                'http://azubu.tv/username': 'Faker'
            };
            
            Channels.update(req, res);
        });
        it('Allows adding a brick by type.', function (done) {
            var req = createMessage('/channels/' + n + '/image', {
                username: n,
                desc: 'image'
            }, JSON.parse(JSON.stringify(bricks.image)));
            
            req.auth = {
                sub: -1,
                'http://azubu.tv/username': n
            };
            
            Channels.add(req, {
                end: function (r) {
                    r = JSON.parse(r);
                    assert.equal(r.length, 4);
                    assert.equal(r[3].item.url, bricks.image.item.url);
                    done();
                }
            });
        });
        it('Prevents unauthorized users from adding.', function (done) {
            var req = createMessage('/channels/' + n + '/image', {
                username: n,
                desc: 'image'
            }, JSON.parse(JSON.stringify(bricks.image))), res = {
                end: function () {
                    assert.equal(res.statusCode, 401);
                    done();
                }
            };
            
            req.auth = {
                sub: '313614',
                'http://azubu.tv/username': 'Faker'
            };
            
            Channels.add(req, res);
        });
        it('Allows removing a brick by index.', function (done) {
            var req = createMessage('/channels/' + n + '/1', {
                username: n,
                desc: '1'
            });
            
            req.auth = {
                sub: -1,
                'http://azubu.tv/username': n
            };
            
            Channels.remove(req, {
                end: function (r) {
                    r = JSON.parse(r);
                    assert.equal(r.length, 3);
                    assert.equal(r[0].type, 'social');
                    done();
                }
            });
        });
        it('Allows removing a brick by index and type.', function (done) {
            var req = createMessage('/channels/' + n + '/image/1', {
                username: n,
                desc: 'image',
                index: '1'
            });
            
            req.auth = {
                sub: -1,
                'http://azubu.tv/username': n
            };
            
           Channels.remove(req, {
                end: function (r) {
                    r = JSON.parse(r);
                    assert.equal(r.length, 2);
                    assert.equal(r[0].type, 'social');
                    assert.equal(r[1].item.url, bricks.image.item.url);
                    done();
                }
            });
        });
        it('Prevents unauthorized users from removing.', function (done) {
            var req = createMessage('/channels/' + n + '/1', {
                username: n,
                desc: '1'
            }), res = {
                end: function () {
                    assert.equal(res.statusCode, 401);
                    done();
                }
            };
            
            req.auth = {
                sub: '313614',
                'http://azubu.tv/username': 'Faker'
            };
            
            Channels.remove(req, res);
        });
    });
});