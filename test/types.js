var assert = require('assert'),
    Types = require('../lib/types');

describe('Types', function () {
    it('Is well-defined.', function () {
        assert.notEqual(Types, null);
        assert.notEqual(Types, undefined);
    });
    it('Validates simple types.', function () {
        var map = {
            first: String,
            last: String,
            age: Number,
            birth: Date
        };
        assert(Types.match(map, {
            first: 'Test',
            last: 'McTesterson',
            age: 35,
            birth: new Date(1981, 2, 1)
        }));
    });
    it('Rejects unmatched simple types.', function () {
        var map = {
            first: String,
            last: String,
            age: Number,
            birth: Date,
            employed: Boolean
        };
        assert(!Types.match(map, {
            first: 'Test',
            last: 'McTesterson',
            age: 'thirty-five',
            birth: new Date(1981, 2, 1),
            employed: true
        }));
        assert(!Types.match(map, {
            first: 'Test',
            last: 'McTesterson',
            middle: 'T.',
            age: 35,
            birth: new Date(1981, 2, 1),
            employed: false
        }));
    });
    it('Validates nested types.', function () {
        var map = {
            name: {
                first: String,
                last: String
            },
            age: Number,
            birth: Date
        };
        assert(Types.match(map, {
            name: {
                first: 'Test',
                last: 'McTesterson'
            },
            age: 35,
            birth: new Date(1981, 2, 1)
        }));
    });
    it('Rejects unmatched nested types.', function () {
        var map = {
            name: {
                first: String,
                last: String
            },
            age: Number,
            birth: Date
        };
        assert(!Types.match(map, {
            name: {
                first: 'Test',
                last: 'McTesterson',
            },
            age: 'thirty-five',
            birth: new Date(1981, 2, 1)
        }));
        assert(!Types.match(map, {
            name: {
                first: 'Test',
                last: 'McTesterson',
                middle: 'T.'
            },
            age: 35,
            birth: new Date(1981, 2, 1)
        }));
    });
    it('Validates types inside arrays.', function () {
        var map = {
            name: String,
            items: [{
                name: String,
                quantity: Number
            }]
        };
        assert(Types.match(map, {
            name: 'Inventory',
            items: [
                { name: 'Item 1', quantity: 1 },
                { name: 'Item 2', quantity: 3 }
            ]
        }));
    });
    it('Rejects unmatched array items.', function () {
        var map = {
            name: String,
            items: [{
                name: String,
                quantity: Number
            }]
        };
        assert(!Types.match(map, {
            name: 'Inventory',
            items: [
                { name: 'Item 1', quantity: 1 },
                { name: 'Item 2', quantity: 'three' }
            ]
        }));
        assert(!Types.match(map, {
            name: 'Inventory',
            items: [
                { name: 'Item 1', quantity: 1 },
                { name: 'Item 2', quantity: 3, status: 'ordered' }
            ]
        }));
    });
    it('Validates type sequences inside arrays.', function () {
        var map = {
            name: String,
            items: [{
                name: String,
                quantity: Number
            }, {
                order: Number,
                quantity: Number
            }]
        };
        assert(Types.match(map, {
            name: 'Inventory',
            items: [
                { name: 'Item 1', quantity: 1 },
                { order: 1, quantity: 1 },
                { name: 'Item 2', quantity: 3 },
                { order: 2, quantity: 3 }
            ]
        }));
    });
    it('Rejects unmatched array sequences.', function () {
        var map = {
            name: String,
            items: [{
                name: String,
                quantity: Number
            }]
        };
        assert(!Types.match(map, {
            name: 'Inventory',
            items: [
                { name: 'Item 1', quantity: 1 },
                { name: 'Item 2', quantity: 3, status: 'ordered' }
            ]
        }));
    });
});