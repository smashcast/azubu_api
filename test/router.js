var assert = require('assert'),
    http = require('http'), 
    verbs =  ['get', 'post', 'put', 'delete', 'options'],
    Router = require('../lib/router');

function createMessage (method, path) {
    var message = new http.IncomingMessage();
    message.method = method.toUpperCase();
    message.url = path;

    return message;
}

function routerObject () {
    var router = new Router();

    it('Is well-defined.', function () {
        assert.notEqual(router, undefined);
        assert.notEqual(router, null);
    });
    verbs.concat('apply').forEach(function (v) {
        it('Contains the \'' + v + '\' method.', function () {
            assert.equal(typeof(router[v]), 'function');
        });
    });
}

function RouterVerb (verb) {
    var router = new Router(),
        method = router[verb].bind(router);

    function f () { return 'f'; }
    function g () { return 'g'; }

    it('Accepts one path and at least one function argument.', function () {
        assert.doesNotThrow(function () { method('/test', f); });
        assert.doesNotThrow(function () { method('/test', f, g); });
        assert.throws(function () { method('/test'); });
        assert.throws(function () { method(f)});
    });
    it('Is call-chainable.', function () {
        assert.equal(typeof(method('/test', f)[verb]), 'function');
    });
    it('Routes requests appropriately.', function () {
        method('/f', f);
        method('/g', g);
        assert.equal(router.apply(createMessage(verb, '/f'))(), 'f');
        assert.equal(router.apply(createMessage(verb, '/g'))(), 'g');
        assert.equal(router.apply(createMessage(verb, '/h')), null);
    });
    it('Routes requests appropriately, ignoring query params.', function () {
        method('/f', f);
        assert.equal(router.apply(createMessage(verb, '/f?q=1'))(), 'f');
    });
    it('Captures path stamps correctly.', function (done) {
        function stamp (req) {
            assert.equal(req.stamps.id, '1');
        }
        function multiStamp (req) {
            assert.equal(req.stamps.s1, '1');
            assert.equal(req.stamps.s2, '2');
            done();
        }

        method('/stamp/:id', stamp);
        method('/ms/:s1/and/:s2', multiStamp);
        router.apply(createMessage(verb, '/stamp/1'))();
        router.apply(createMessage(verb, '/ms/1/and/2', multiStamp))();
    });
    if (verb == 'get') {
        it('Captures optional path stamps correctly.', function () {
            function f (req) { return req.stamps.id; }
            function multiStamp2 (req) {
                var s1 = req.stamps.s1 || '0', s2 = req.stamps.s2 || '0';
                return parseInt(s1) + parseInt(s2);
            }

            method('/opt/:id?', f);
            assert.equal(router.apply(createMessage(verb, '/opt'))(), null);
            assert.equal(router.apply(createMessage(verb, '/opt/1'))(), '1');
            method('/opt2/:s1/and/:s2?', multiStamp2);
            assert.equal(router.apply(createMessage(verb, '/opt2/1/and'))(), 1);
            assert.equal(router.apply(
                createMessage(verb, '/opt2/1/and/2'))(), 3);
        });
    }
    it('Allows for multiple path responders.', function () {
        var calls = 0;

        function inc (req, res, next) { ++calls; next(); }

        method('/inc', inc, inc, inc, inc);
        router.apply(createMessage(verb, '/inc'))();
        assert.equal(calls, 4);
    });
    it('Allows for the prefixing of a set of path responders.', function () {
	var calls = 0,
	    pfx = router.with('/pfx'),
	    m = pfx[verb].bind(pfx);

	function f () { ++calls; }

	m('/first', f);
	m('/second', f);

	router.apply(createMessage(verb, '/pfx/first'))();
	router.apply(createMessage(verb, '/pfx/second'))();
	assert.equal(calls, 2);
    });
}

describe('Router', function () {
    describe('Object', routerObject);
    verbs.forEach(function (v) {
        describe(v.toUpperCase(), RouterVerb.bind(this, v));
    });
});
