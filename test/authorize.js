var assert = require('assert'),
    http = require('http'),
    Auth = require('../lib/authorize'),
    crypto = require('crypto'),
    key = process.env.AZUBU_HS256_KEY,
    testToken = genToken((new Date).valueOf(), (new Date).valueOf() + 2592000);

Auth(key);

function genToken (iat, exp) {
    var now = (new Date).valueOf(),
	hdr = new Buffer(JSON.stringify({
	    alg: 'HS256',
	    typ: 'jwt'
	}), 'utf8').toString('base64'),
	bdy = new Buffer(JSON.stringify({
	    sub: '313614',
	    'http://azubu.tv/username': 'Faker',
	    'iat': iat,
	    'exp': exp,
	}), 'utf8').toString('base64'),
	hmac = crypto.createHmac('sha256', key);

    return hdr + '.'
	+ bdy + '.'
	+ hmac.update(hdr + '.' + bdy).digest('base64');
}


function failShim () {
    return {
        setHeader: function (name, value) {
            assert(false);
        },
        end: function () {
            assert(false);
        }
    };
}

function rejectShim () {
    return {
	setHeader: function (name, value) {
            assert.equal(name, 'www-authenticate');
            assert.equal(value, 'bearer');
	},
	end: function () {
            assert.equal(this.statusCode, 401);
	}
    };
}

function createMessage (token) {
    var message = new http.IncomingMessage();
    message.headers.authorization = 'bearer ' + token;

    return message;
}

function restrict () {
    it('Contains a restrict method.', function () {
        assert.equal(typeof(Auth.restrict), 'function');
    });
    it('Accepts a JWT and modifies the request.', function () {
	var req = createMessage(testToken);
	Auth.restrict(req, failShim());
	assert.notEqual(req.auth, null);
	assert.equal(req.auth.sub, '313614');
	assert.equal(req.auth['http://azubu.tv/username'], 'Faker');
    });
    it('Allows the restrict method to act as middleware.', function (done) {
	Auth.restrict(createMessage(testToken), failShim(), function () {
            assert(true);
            done();
	});
    });
    it('Rejects invalid tokens.', function () {
        Auth.restrict(createMessage('fail'), rejectShim(), function () {
            assert(false);
        });
    });
    it('Rejects untimely tokens.', function () {
	var d = (new Date).valueOf(),
	    pastToken = genToken(d - 2, d - 1),
	    futureToken = genToken(d + 1, d + 2),
	    invalidToken = genToken(d, d - 1);

	function f () {
	    assert(false);
	}

	Auth.restrict(createMessage(pastToken), rejectShim(), f);
	Auth.restrict(createMessage(futureToken), rejectShim(), f);
	Auth.restrict(createMessage(invalidToken), rejectShim(), f);
    });
}

function check () {
    it('Contains a check method.', function () {
        assert.equal(typeof(Auth.check), 'function');
    });
    it('Accepts a JWT and modifies the request.', function () { 
	var req = createMessage(testToken);
	Auth.check(req, failShim());
	assert.notEqual(req.auth, null);
	assert.equal(req.auth.sub, '313614');
	assert.equal(req.auth['http://azubu.tv/username'], 'Faker');
    });
    it('Allows the check method to act as middleware.', function (done) {
	Auth.check(createMessage(testToken), failShim(), function () {
            assert(true);
            done();
	});
    });
    it('Allows unauthorized access.', function () {
        Auth.check(createMessage(), {}, function () {
            assert(true);
        });
    });
    it('Allows untimely tokens.', function () {
	var d = (new Date).valueOf(),
	    pastToken = genToken(d - 2, d - 1),
	    futureToken = genToken(d + 1, d + 2),
	    invalidToken = genToken(d, d - 1);

	function f () {
	    assert(true);
	}

	Auth.check(createMessage(pastToken), {}, f);
	Auth.check(createMessage(futureToken), {}, f);
	Auth.check(createMessage(invalidToken), {}, f);
    });
}

describe('Authorize', function (){
    it('Is well-defined.', function () {
        assert.notEqual(Auth, undefined);
        assert.notEqual(Auth, null);
    });
    describe('Restrict', restrict);
    describe('Check', check);
});
