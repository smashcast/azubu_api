var assert = require('assert'),
    UsersProvider = require('../resources/providers/users-provider'),
    neo4j = require('neo4j'),
    connection = process.env.AZUBU_NEO4J_URL,
    db = new neo4j.GraphDatabase(connection);
    
function retrieving (users) {
    var count, list, faker, mikiocute, fFaker, fCount,
        p = 'u.id AS id, u.username AS username',
        d = p + ', u.email AS email';
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (u:User) WITH u RETURN count(u) AS count;'
        }, function (err, res) {
            count = res[0].count;
            done();
        });
    });
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (u:User) RETURN ' + p + ' LIMIT 100;'
        }, function (err, res) {
            list = res;
            done();
        });
    });
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (u:User {searchName: {s}}) RETURN ' + d + ';',
            params: { s: 'faker' }
        }, function (err, res) {
            faker = res;
            done();
        });
    });
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (:User {searchName: {s}})-[:Following]->(u:User) ' + 
                'RETURN ' + p + ' LIMIT 5;',
            params: { s: 'mikiocute' }
        }, function (err, res) {
            mikiocute = res;
            done();
        });
    });
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (u:User)-[:Following]->(:User {searchName: {s}}) ' +
                'RETURN ' + p + ' LIMIT 5;',
            params: { s: 'faker' }
        }, function (err, res) {
            fFaker = res;
            done();
        });
    });
    
    before(function (done) {
        db.cypher({
            query: 'MATCH (u:User)-[:Following]->(:User {searchName: {s}}) ' +
                'WITH u RETURN count(u) AS count;',
            params: { s: 'faker' }
        }, function (err, res) {
            fCount = res[0].count;
            done();
        });
    });
    
    it('Returns a user count.', function (done) {
        users.count(function (err, res) {
            assert.equal(count, res);
            done();
        });
    });
    it('Returns a user list.', function (done) {
        var skip = 20, limit = 5;
        
        users.list(function (err, l) {
            var i = skip;
            
            for (; i < skip + limit; ++i) {
                assert.deepEqual(list[i], l[i - skip]);
            }
            done();
        }, skip, limit);
    });
    it('Limits user list length by default.', function (done) {
        users.list(function (err, users) {
            assert.equal(users.length, 100);
            done();
        });
    });
    it('Returns appropriate users.', function (done) {
        users.get('faker', function (err, u) {
            assert.equal(faker.username, u.username);
            assert.equal(faker.id, u.id);
            done();
        });
    });
    it('Returns appropriate users\' detailed view.', function (done) {
        users.getDetail('faker', function (err, u) {
            assert.deepEqual(faker, u);
            done();
        });
    });
    it('Returns a users followed by the given user.', function (done) {
        users.following('mikiocute', function (err, f) {
            assert.deepEqual(mikiocute, f);
            done();
        }, 0, 5);
    });
    it('Returns a specific user followed by a given user.', function (done) {
        users.followingDetail('mikiocute', 'faker', function (err, f) {
            assert.equal(faker.id, f.id);
            assert.equal(faker.username, f.username);
            done();
        });
    });
    it('Returns a list of users following the given user.', function (done) {
        users.followed('faker', function (err, f) {
            assert.deepEqual(fFaker, f);
            done();
        }, 0, 5);
    });
    it('Returns a count of users following the given user.', function (done) {
        users.followedCount('faker', function (err, count) {
            assert.equal(fCount, count);
            done();
        });
    });
}

function modifying (users) {
    after(function (done) {
        db.cypher({
            query: 'MATCH (u:User { searchName: {s} }) ' +
                'OPTIONAL MATCH (u)-[f:Following]-() ' +
                'WITH u, f SET u.id = -1, u.username = {s}, u.email = {e} ' +
                'DELETE f;',
            params: {
                s: 'unit_tester',
                e: 'unit@tester.com'
            }
        }, done);
    });
    
    it('Allows for changes to a user\'s record.', function (done) {
        users.update('unit_tester', { 
            email: 'test@change.com' 
        }, function (err, d) {
            db.cypher({
                query: 'MATCH (u:User { searchName: {u} }) RETURN u.email;',
                params: { u: 'unit_tester' }
            }, function (err, res) {
                assert.equal(res[0]['u.email'], 'test@change.com');
                done();
            });
        });
    });
    it('Does not allow a user to change their name or id.', function (done) {
        users.update('unit_tester', {
            id: 12345, username: 'change_tester'
        }, function (err, res) {
            assert.notEqual(res.id, 12345);
            assert.notEqual(res.username, 'change_tester');
            done();
        });
    });
    it('Does not allow a user to add arbitrary fields.', function (done) {
        users.update('unit_tester', {
            fail: 'fail'
        }, function (err, d) {
            assert.equal(d.fail, undefined);
            done();
        });
    });
    it('Allows for the following of a user.', function (done) {
        users.follow('unit_tester', 'faker', function (err, d) {
            assert(d);
            db.cypher({
                query: 'MATCH (:User { searchName: {u} })' +
                    '-[f:Following]->(:User { searchName: {f} }) ' +
                    'RETURN f;',
                params: { u: 'unit_tester', f: 'faker' }
            }, function (err, res) {
                assert(res.length);
                done();
            });
        });
    });
    it('Allows for the unfollowing of a user.', function (done) {
        users.unfollow('unit_tester', 'faker', function (err, d) {
            assert(d);
            db.cypher({
                query: 'MATCH (:User { searchName: {u} })' +
                    '-[f:Following]->(:User { searchName: {f} }) ' +
                    'RETURN f;',
                params: { u: 'unit_tester', f: 'faker' }
            }, function (err, res) {
                assert(!res.length);
                done();
            });
        });
    });
}
    
describe('Users Provider', function () {
    var users = new UsersProvider(connection);
    
    it('Is well-defined.', function () {
        assert.notEqual(users, undefined);
        assert.notEqual(users, null);
    });
    describe('Retrieving', function () {
        retrieving(users);
    });
    describe('Modifying', function () {
        modifying(users);
    });
});
