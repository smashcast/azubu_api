var url = require('url'),
    Auth = require('../lib/authorize'),
    provider;

function getParams (path) {
    var query = url.parse(path, true).query || {}, params = {};

    params.count = query.count != undefined;
    if (query.limit) {
        params.limit = parseInt(query.limit);
    }
    if (query.skip) {
        params.skip = parseInt(query.skip);
    }
    
    return params;
}

module.exports = function (p) {
    provider = p;
};

module.exports.get = function (req, res) {
    var params = getParams(req.url);
    
    if (req.stamps.username) {
        req.stamps.username = req.stamps.username.toLowerCase();
        if (Auth.match(req, req.stamps.username)) {
            provider.getDetail(req.stamps.username, function (err, data) {
                if (data.length) {
                    res.end(JSON.stringify(data[0]));
                } else {
                    res.statusCode = 404;
                    res.end();
                }
            });
        } else {
            provider.get(req.stamps.username, function (err, data) {
                if (data.length) {
                    res.end(JSON.stringify(data[0]));
                } else {
                    res.statusCode = 404;
                    res.end();
                }
            });
        }
    } else if (params.count) {
        provider.count(function (err, data) {
            res.end(JSON.stringify({ count: data }));
        });
    } else {
        provider.list(function (err, data) {
            res.end(JSON.stringify(data));
        }, params.skip, params.limit);
    }
};

module.exports.following = function (req, res) {
    var params = getParams(req.url),
        username = req.stamps.username.toLowerCase(),
        follow;
    
    if (req.stamps.follow) {
        follow = req.stamps.follow.toLowerCase();
        provider.followingDetail(username, follow, function (err, data) {
            if (data.length) {
                res.end(JSON.stringify(data[0]));
            } else {
                res.statusCode = 404;
                res.end();
            }
        });
    } else if (params.count) {
        provider.following(username, function (err, data) {
            res.end(JSON.stringify({ count: data.length }));
        });
    } else {
        provider.following(username, function (err, data) {
            res.end(JSON.stringify(data));
        }, params.skip, params.limit);
    }
};

module.exports.followed = function (req, res) {
    var params = getParams(req.url),
        username = req.stamps.username.toLowerCase();
    
    if (params.count) {
        provider.followedCount(username, function (err, data) {
            res.end(JSON.stringify({ count: data }));
        });
    } else {
        provider.followed(username, function (err, data) {
            res.end(JSON.stringify(data));
        }, params.skip, params.limit);
    }
};

module.exports.update = function (req, res) {
    var username = req.stamps.username.toLowerCase();
    
    if (Auth.match(req, username)) {
        provider.update(username, req.body, function (err, data) {
            if (data) {
                res.end(JSON.stringify(data));
            } else {
                res.statusCode = 404;
                res.end();
            }
        });
    } else {
        res.statusCode = 401;
        res.end();
    }
};

module.exports.follow = function (req, res) {
    var username = req.stamps.username.toLowerCase(),
        follow = req.stamps.follow.toLowerCase();
        
    if (Auth.match(req, username)) {
        provider.follow(username, follow, function (err, data) {
            res.statusCode = data ? 201 : 404;
            res.end();
        });
    } else {
        res.statusCode = 401;
        res.end();
    }
};

module.exports.unfollow = function (req, res) {
    var username = req.stamps.username.toLowerCase(),
        follow = req.stamps.follow.toLowerCase();
        
    if (Auth.match(req, username)) {
        provider.unfollow(username, follow, function (err, data) {
            res.statusCode = data ? 204 : 404;
            res.end();
        });
    } else {
        res.statusCode = 401;
        res.end();
    }
};