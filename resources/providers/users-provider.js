var neo4j = require('neo4j'),
    defaultLimit = 100,
    fields = [ 'id', 'username' ],
    detailFields = [ 'id', 'username', 'email' ],
    proto;
    
function query (db, query, params, callback) {
    if (!db) {
        return callback('Error: No connection.', null);
    }
    
    db.cypher({ query: query, params: params }, callback);
}

function returnStatement (prefix, fields) {
    var s = [];
    
    fields.forEach(function (f) {
        s.push(prefix + '.' + f + ' AS ' + f);
    });
    
    return 'RETURN ' + s.join(', ');
}

module.exports = function (connection) {
    this.db = new neo4j.GraphDatabase(connection);
};
proto = module.exports.prototype;

proto.count = function (callback) {
    var q = 'MATCH (u:User) WITH u RETURN count(u) AS c;';
    query(this.db, q, undefined, function (err, res) {
        callback(err, res && res[0]['c']);
    });
};

proto.list = function (callback, skip, limit) {
    var q = 'MATCH (u:User) ' + returnStatement('u', fields) +
        ' SKIP {skip} LIMIT {limit};',
        p = { skip: skip || 0, limit: limit || defaultLimit };
    query(this.db, q, p, callback);
};

proto.get = function (username, callback) {
    var q = 'MATCH (u:User { searchName: {username} }) ' + 
        returnStatement('u', fields) + ';',
        p = { username: username };
    query(this.db, q, p, callback);
};

proto.getDetail = function (username, callback) {
    var q = 'MATCH (u:User { searchName: {username} }) ' +
        returnStatement('u', detailFields) + ';',
        p = { username: username };
    query(this.db, q, p, callback);
};

proto.following = function (username, callback, skip, limit) {
    var q = 'MATCH (:User { searchName: {username} })' +
        '-[:Following]->(f:User) ' + 
        returnStatement('f', fields) + ' SKIP {skip} LIMIT {limit};',
        p = { 
            username: username, 
            skip: skip || 0, 
            limit: limit || defaultLimit 
        };
    query(this.db, q, p, callback);
};

proto.followingDetail = function (username, follow, callback) {
    var q = 'MATCH (:User { searchName: {username} })' +
        '-[:Following]->(f:User { searchName: {follow} }) ' +
        returnStatement('f', fields) + ';',
        p = {
            username: username,
            follow: follow
        };
    query(this.db, q, p, callback);
};

proto.followed = function (username, callback, skip, limit) {
    var q = 'MATCH (u:User)-[:Following]->(:User { searchName: {username} }) ' +
        returnStatement('u', fields) + ' SKIP {skip} LIMIT {limit};',
        p = {
            username: username,
            skip: skip || 0,
            limit: limit || defaultLimit
        };
    query(this.db, q, p, callback);
};

proto.followedCount = function (username, callback) {
    var q = 'MATCH (u:User)-[:Following]->(:User { searchName: {username} }) ' +
        'RETURN count(u) AS c;',
        p = {
            username: username
        };
    query(this.db, q, p, function (err, res) {
        callback(err, res && res[0]['c']);
    });
};

proto.update = function (username, data, callback) {
    var q = 'MATCH (u:User { searchName: {username} }) ', f,
        s = [],
        p = { username: username };
    
    for (f in data) {
        if (detailFields.indexOf(f) != -1 && f != 'id' && f != 'username') {
            s.push('u.' + f + ' = ' + '{' + f + '}');
            p[f] = data[f];
        }
    }
    
    if (s.length) {
        q += 'WITH u SET ' + s.join(', ') + ' ';
    }
    q += returnStatement('u', detailFields) + ';';
    
    query(this.db, q, p, function (err, res) {
        callback(err, res && res[0]);
    });
};

proto.follow = function (username, followName, callback) {
    var q = 'MATCH (u:User { searchName: {username} }), ' + 
        '(f:User { searchName: {followName} }) ' +
        'CREATE (u)-[:Following]->(f) RETURN u, f;',
        p = { username: username, followName: followName };
    query(this.db, q, p, function (err, res) {
        callback(err, !!res.length);
    });
};

proto.unfollow = function (username, unfollowName, callback) {
    var q = 'MATCH (u:User { searchName: {username} })' +
        '-[f:Following]->(:User { searchName: {unfollowName} }) ' +
        'DELETE f RETURN u;',
        p = { username: username, unfollowName: unfollowName };
    
    query(this.db, q, p, function (err, res) {
        callback(err, !!res.length);
    });
};