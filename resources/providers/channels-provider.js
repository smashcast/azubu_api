var defaultLimit = 100,
    Types = require('../../lib/types');
    
function getIndex (list, type, index) {
    var i = 0, k = 0;
    
    if (!type) {
        return index - 1;
    }
    
    for (; i < list.length; ++i) {
        if (list[i].type == type && ++k == index) {
            break;
        }
    }
    
    return i;
}

module.exports = function (col) {
    this.col = col;
};

module.exports.prototype.count = function (callback) {
    this.col.aggregate([
        { $group: { _id: '$searchName' } },
        { $group: { _id: 1, count: { $sum: 1 } } }
    ], function (err, res) {
        callback(err, res && res[0] && parseInt(res[0].count));
    });
};

module.exports.prototype.list = function (callback, skip, limit) {
    this.col.aggregate([ 
        { $group: { 
            _id: '$searchName', 
            uid: { $first: '$userId' },
            username: { $first: '$username' } 
        } }, 
        { $sort: { _id: 1 } },
        { $skip: skip || 0 }, { $limit: limit || defaultLimit }, 
        { $project: { id: '$uid', username: '$username', _id: 0 } }
    ], callback);
};

module.exports.prototype.detail = function (name, callback) {
    this.col.find(
        { searchName: name.toLowerCase() }, 
        { _id: 0, searchName: 0 }
    ).sort({ _id: 1 }).toArray(callback);
};

module.exports.prototype.brick = function (name, index, callback) {
    this.col.aggregate([
        { $match: { searchName: name.toLowerCase() } }, 
        { $sort: { _id: 1 } },
        { $skip: index - 1 }, 
        { $limit: 1 } 
    ], function (err, res) {
        if ((res = res && res[0])) {
            delete res._id;
            delete res.searchName;
        }
        callback(err, res);
    });
};

module.exports.prototype.bricksByType = function (name, type, callback) {
    this.col.find({ 
        searchName: name.toLowerCase(), 
        type: type.toLowerCase() },
        { _id: 0, searchName: 0 }
    ).sort({ _id: 1 }).toArray(callback);
};

module.exports.prototype.brickByType = function (name, type, index, callback) {
    this.col.aggregate([
        { $match: { 
            searchName: name.toLowerCase(), 
            type: type.toLowerCase() 
        } }, 
        { $sort: { _id: 1 } },
        { $skip: index - 1 },
        { $limit: 1 }
    ], function (err, res) {
        if (res && res[0]) {
            res = res[0];
            delete res._id;
            delete res.searchName;
        }
        callback(err, res);
    });
};

module.exports.prototype.addBrick = function (name, type, data, callback) {
    if (Types[type] && Types[type](data)) {
        data.type = type;
        data.owner = 'user';
        data.username = name;
        data.searchName = name.toLowerCase();
        this.col.insert(data, (function (err, res) {
            this.detail(name, callback);
        }).bind(this));
    } else {
        callback('Malformed brick of type: ' + type + '.');
    }
};

module.exports.prototype.updateBrick = function (name, search, data, callback) {
    this.col.aggregate([
        { $match: { searchName: name.toLowerCase() } },
        { $sort: { _id: 1 } }
    ], (function (err, res) {
        var i = getIndex(res, search.type, search.index), s;
        
        if (!res[i]) {
            callback('Invalid brick.');
        } else if (!Types[res[i].type] || !Types[res[i].type](data)) {
            callback('Malformed brick of type: ' + res[i].type + '.');
        } else {
            for (s in data) {
                res[i][s] = data[s];
            }
            this.col.save(res[i], function (err) {
                callback(err, res);
            });
        }
    }).bind(this));
};

module.exports.prototype.deleteBrick = function (name, search, callback) {
    this.col.aggregate([
        { $match: { searchName: name.toLowerCase() } },
        { $sort: { _id: 1 } }
    ], (function (err, res) {
        var i = getIndex(res, search.type, search.index);
          
        if (res[i]) { 
            this.col.deleteOne({ _id: res[i]._id }, function (err) {
                res.splice(i, 1);
                callback(err, res);
            });
        } else {
            callback('Invalid brick.');
        }
    }).bind(this));
};