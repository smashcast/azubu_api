var url = require('url'),
    Auth = require('../lib/authorize'),
    provider;

function getParams (path) {
    var query = url.parse(path, true).query || {}, params = {};

    params.count = query.count != undefined;
    if (query.limit) {
        params.limit = parseInt(query.limit);
    }
    if (query.skip) {
        params.skip = parseInt(query.skip);
    }
    
    return params;
}

module.exports = function (p) {
    provider = p;
};

module.exports.get = function (req, res) {
    var params = getParams(req.url);
    
    if (req.stamps.username) {
        req.stamps.username = req.stamps.username.toLowerCase();
        provider.detail(req.stamps.username, function (err, data) {
            if (data.length) {
                res.end(JSON.stringify(data));
            } else {
                res.statusCode = 204;
                res.end();
            }
        });
    } else if (params.count) {
        provider.count(function (err, data) {
            res.end(JSON.stringify({ count: data }));
        });
    } else {
        provider.list(function (err, data) {
            res.end(JSON.stringify(data));
        }, params.skip, params.limit);
    }
};

module.exports.bricks = function (req, res) {
    var uname = req.stamps.username.toLowerCase(),
        desc = req.stamps.desc,
        index = req.stamps.index;
        
    if (!isNaN(parseInt(desc))) {
        provider.brick(uname, parseInt(desc), function (err, data) {
            if (data) {
                res.end(JSON.stringify(data));
            } else {
                res.statusCode = 404;
                res.end();
            }
        });
    } else if (!isNaN(parseInt(index))) {
        index = parseInt(index);
        provider.brickByType(uname, desc, index, function (err, data) {
            if (data) {
                res.end(JSON.stringify(data));
            } else {
                res.statusCode = 404;
                res.end();
            }
        });
    } else {
        provider.bricksByType(uname, desc, function (err, data) {
            if (data.length) {
                res.end(JSON.stringify(data));
            } else {
                res.statusCode = 404;
                res.end();
            }
        });
    }
};

module.exports.update = function (req, res) {
    var uname = req.stamps.username.toLowerCase(),
        search = {
            index: parseInt(req.stamps.index) || parseInt(req.stamps.desc)
        };
    
    if (req.stamps.index) {
        search.type = req.stamps.desc;
    }
    
    if (!Auth.match(req, uname)) {
        res.statusCode = 401;
        res.end();
    } else if (search.index) {
        provider.updateBrick(uname, search, req.body, function (err, r) {
            if (err == 'Invalid brick.') {
                res.statusCode = 404;
                res.end();
            } else if (err) {
                res.statusCode = 400;
                res.end();
            } else {
                res.end(JSON.stringify(r));
            }
        });
    } else {
        res.statusCode = 400;
        res.end();
    }
};

module.exports.add = function (req, res) {
    var uname = req.stamps.username.toLowerCase(),
        desc = req.stamps.desc;
    
    if (!Auth.match(req, uname)) {
        res.statusCode = 401;
        res.end();
    } else {
        provider.addBrick(uname, desc, req.body, function (err, r) {
            if (err) {
                res.statusCode = 400;
                res.end();
            } else {
                res.end(JSON.stringify(r));
            }
        });
    }
};

module.exports.remove = function (req, res) {
    var uname = req.stamps.username.toLowerCase(),
        search = {
            index: parseInt(req.stamps.index) || parseInt(req.stamps.desc)
        };
    
    if (req.stamps.index) {
        search.type = req.stamps.desc;
    }
    
    if (!Auth.match(req, uname)) {
        res.statusCode = 401;
        res.end();
    } else if (search.index) {
        provider.deleteBrick(uname, search, function (err, r) {
            if (err) {
                res.statusCode = 404;
                res.end();
            } else {
                res.end(JSON.stringify(r));
            }
        });
    } else {
        res.statusCode = 400;
        res.end();
    }
};